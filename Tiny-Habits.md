# **Tiny Habits**

# I. Tiny Habits-BJ Fogg

Video - 17:24 seconds - https://www.youtube.com/watch?v=AdKUJxjn-R8

## **1. Your takeaways from the video (Minimum 5 points)**

- Relying on motivation alone does not allow a person to change their long-term habits
- One can create many tiny habits to change their long time habits
- Indulge in tiny habits just after an already existing habit, as it can act as a trigger
- One should determine what one needs to change in their life and break it down into tiny habits, then should put them in the right spots
- These habits should gradually be increased to convert them into a long-term change

# II. Tiny Habits by BJ Fogg-Core Message

Video: 9:15 minutes - https://www.youtube.com/watch?v=S_8e-6ZHKLs

## **2. Your takeaways from the video in as much detail as possible**

- B=MAP, where B is **Behaviour**, M is **Motivation**, A is **Ability** , P is **Prompt**
- Tiny habits include the following three parts:

  1. Shrink the Behaviour: Shrink the behavior by reducing the quantity, or doing one step at a time. A shrunk behavior requires very little motivation for its completion. The behavior should be easy to do within 30 seconds or less.

  2. Identify an action prompt: Prompts can be either external, internal or action prompts. External and internal prompts are easy to ignore and are disruptive and demotivating. Do the tiny habits in-between other regular habits to do them in a flow.

  3. Grow habits with some shine: Learning to celebrate after a tiny win is the most critical component of habit development. Doing so increases one's confidence and motivation for doing that habit again.

## **3. How can you use B = MAP to make making new habits easier?**

From this equation, we can understand that motivation, ability, and prompt directly affects our behavior. If the behavior is small, we need only small motivation, small ability, and prompt to execute that behavior. So, I should create tiny habits in between regularly done habits, so that the regular habit itself acts as a prompt, motivation required is less and the ability to do the action is sufficient. Then I should gradually increase the time invested in these habits, to create a new habit entirely.

## **4. Why is it important to "Shine" or Celebrate after each successful completion of a habit?**

It is important to Shine because when we appreciate ourselves each time for completing a small task, our motivation and confidence for doing the task next time increases, which in turn helps us to build a new habits.

# III. 1% Better Every Day Video

Video - 24:36 minutes - https://www.youtube.com/watch?v=mNeXuCYiE0U

## **5. Your takeaways from the video (Minimum 5 points)**

- Give your goals a time and place to live in the world
- Environment can significantly act as a driver for one's behavior
- One can prevent oneself from indulging in unwanted behaviors by creating barriers to that behaviors
- Two-minute rule: If the good habit can be done in two minutes, do it then and there itself
- Optimize for the starting line, not the finish line
- We need to like the habit that we are trying to build to have the motivation to build the same
- Never break the chain of tiny habits

# IV. Book Summary of Atomic Habits

Video - 11:11 minutes - https://www.youtube.com/watch?v=YT7tQzmGRLA

## **6. Write about the book's perspective on habit formation from the lens of identity, processes, and outcomes?**

- Identity change is the north star of habit change
- Most of us work from outcomes to identities rather than identity to the outcome
- The ultimate form of intrinsic motivation is when a habit becomes part of our identity
- Solving problems based on outcomes or results only solves them temporarily
- To solve problems in the longer term, we need to change our identity

## **7. Write about the book's perspective on how to make a good habit easier?**

- There should be a few steps between good behavior and yourself
- To make a good habit, we can split the process into four stages:

1. Cue: Triggers the brain to initiate an action. Make the trigger obvious and easy to reach.
2. Craving: Provides the motivational force. Make the habit attractive to increase the craving.
3. Response: The action or habit that we perform. Reduce the friction and make it easy.
4. Reward: The end goal. Make the behavior immediately satisfying.

## **8. Write about the book's perspective on making a bad habit more difficult?**

- To make a bad habit difficult, we should increase the steps for the bad habit
- The four stages mentioned above can be reversed as follows:

1. Make the cues invisible and harder to reach
2. Make the habit ugly to decrease the craving and motivation to indulge in the behavior.
3. Increase the friction to indulge in the behavior
4. Make the behavior immediately unsatisfying

# V. Reflection:

## **9. Pick one habit that you would like to do more of. What are the steps that you can take to make it make the cue obvious or the habit more attractive or easy and or response satisfying?**

I need to exercise more. To make the cue obvious, I should do it immediately after work hours. I should make the habit more attractive by thinking of the health benefits one can get by exercising regularly, and I should reward myself with a sound night of sleep after exercise.

## **10. Pick one habit that you would like to eliminate or do less of. What are the steps that you can take to make it make the cue invisible or the process unattractive or hard or the response unsatisfying?**

I need to consume less coffee. To make the cue invisible, I should try to stay away from places that serve coffee. I should make the habit unattractive by thinking of how it affects my sleep, and I should make the behavior unsatisfying by making the coffee more bitter.
